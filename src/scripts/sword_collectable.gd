extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	$Cube.rotation_degrees = $Cube.rotation_degrees + Vector3(0, 1, 0)


func _on_Area_body_entered(body):
	if body.get_name() == 'Player':
		visible = false
		PlayerVariables.sword_active = true
