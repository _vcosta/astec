extends Spatial

func _ready():
	if PlayerVariables.main_scene_checkpoint != null:
		$Player.transform = PlayerVariables.main_scene_checkpoint
	else:
		PlayerVariables.main_scene_checkpoint = $Player.transform