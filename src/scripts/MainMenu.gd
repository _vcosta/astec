extends MarginContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var focus = 0
var nuevojogo_default = load("res://assets/main_menu/label_new_game.png")
var nuevojogo_selected = load("res://assets/main_menu/label_new_game_selected.png")
var opciones_default = load("res://assets/main_menu/label_options.png")
var opciones_selected = load("res://assets/main_menu/label_options_selected.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	change_textures()
	PlayerVariables.reset_all()
	

func _process(delta):
	rotate_background()	
	if Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_down"):
		focus = (focus + 1) % 2
		change_textures()
	if Input.is_action_just_pressed('jump'):
		if focus == 0:
			get_tree().change_scene("res://src/scenes/Game.tscn")
			PlayerVariables.shield_active = false
			PlayerVariables.sword_active = false
		else:
			$"HBoxContainer/input BG".visible = not $"HBoxContainer/input BG".visible

func rotate_background():
	$bg_01.rotation_degrees = $bg_01.rotation_degrees + 0.02
	$bg_02.rotation_degrees = $bg_02.rotation_degrees - 0.1
	$bg_03.rotation_degrees = $bg_03.rotation_degrees + 0.2
	
func change_textures():
	if focus == 0:
		$HBoxContainer/VBoxContainer/VBoxContainer/NewGame.texture = nuevojogo_selected
		$HBoxContainer/VBoxContainer/VBoxContainer/Options.texture = opciones_default
	else:
		$HBoxContainer/VBoxContainer/VBoxContainer/NewGame.texture = nuevojogo_default
		$HBoxContainer/VBoxContainer/VBoxContainer/Options.texture = opciones_selected
	
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
