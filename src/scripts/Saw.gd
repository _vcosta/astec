extends KinematicBody

export var path_name = ""
export var move = false
export var speed = 0
var path
var path_index = 0
var target = Vector3()
var velocity = Vector3()
# Called when the node enters the scene tree for the first time.
func _ready():
	if move:
		path = get_parent().get_node(path_name).get_children()
		get_new_target()

func _process(delta):
	$Cylinder.rotation_degrees = $Cylinder.rotation_degrees + Vector3(0, -5, 0)

func get_new_target():
	target = path[path_index].translation
	
func _physics_process(delta):
	if move:
		velocity = (target - translation).normalized() * speed
	    # rotation = velocity.angle()
		if (target - translation).length() > 1:
			move_and_slide(velocity)
		else:
			path_index = ( path_index + 1 ) % path.size()
			get_new_target()


func _on_Area_body_entered(body):
	if body.get_name() == 'Player':
		get_parent().get_parent().get_node('Player').life -= 10
