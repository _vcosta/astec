extends MeshInstance

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var velocity = Vector3(
		0.2,
		0,
		0)
		
var t
# Called when the node enters the scene tree for the first time.

func _ready():
	t = get_transform()
	
func _process(delta):
	t.origin += velocity * delta
	set_transform(t)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
