extends Spatial

var checkpoint

func _ready():
	checkpoint = $Player.transform

func _on_ground_body_entered(body):
	$Player.transform = checkpoint
	$Player.life -= 10
