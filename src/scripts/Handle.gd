extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var pushed = false
signal cage_up
signal cage_down

# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect('cage_up', get_parent().get_node('Cage'), '_on_Cage_Up')
	self.connect('cage_down', get_parent().get_node('Cage'), '_on_Cage_Down')

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Area_body_entered(body):
	if body.get_name() == 'Player':
			$Sprite3D.visible = true
			
func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("action") and $Sprite3D.visible :
		print('Handle pushd')
		if not pushed:
			$AnimationPlayer.play('default')
			pushed = true
		else:
			$AnimationPlayer.play_backwards('default')
			pushed = false

func _on_Area_body_exited(body):
	if body.get_name() == 'Player':
			$Sprite3D.visible = false

func _on_AnimationPlayer_animation_finished(anim_name):
	if pushed:
		emit_signal("cage_up")
	else:
		emit_signal("cage_down")

