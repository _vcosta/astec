extends KinematicBody

const FLOOR_NORMAL = Vector3(0.0, 1.0, 0.0)

export var speed := 7.0
export var gravity := 30.0
export var jump_force := 12.0

var velocity_y := 0.0
var direction = 0
var last_velocity = Vector2(0,0)
var life = 135
var aim = Vector2()
var life_bar
var on_floor = false

# Called when the node enters the scene tree for the first time.
func _ready():
	life_bar = $GUI/HBoxContainer/Bars/EnergyBar/TextureProgress
	aim =  Vector2(1,1)
	$RotationOffset/Target.look_at_from_position( to_global($RotationOffset.translation),
													  to_global($RotationOffset.translation) + 
												        Vector3(-aim.x, 0, aim.y),
												        Vector3(0,1,0))
	
func _process(delta):
	
	aim.x = Input.get_action_strength("target_right") - Input.get_action_strength("target_left")
	aim.y = Input.get_action_strength("target_up") - Input.get_action_strength("target_down")
	#aim = aim.rotated(deg2rad(180) + deg2rad(camera_angles[ camera_index ]))
	aim = aim.rotated(deg2rad(180) + rotation.y)
	
	if abs(aim.x) != 0 and abs(aim.y) != 0:
		$RotationOffset/Target.look_at_from_position( to_global($RotationOffset.translation),
													  to_global($RotationOffset.translation) + 
												        Vector3(-aim.x, 0, aim.y),
												        Vector3(0,1,0))
		
	life_bar.value = life
	if life_bar.value <= 35:
		get_tree().change_scene("res://src/scenes/MainMenu.tscn")
		
	
	var ray = $RayCast
	if ray.is_colliding():
		on_floor = true
	else:
		on_floor = false
				
	if Input.is_action_just_pressed("jump"):
		if on_floor:
			velocity_y = jump_force
	#if event.is_action_pressed("camera_change"):
	#	camera_index = ( camera_index + 1 ) % camera_angles.size()
	#	rotation.y = deg2rad(camera_angles[ camera_index ])
	if Input.is_action_pressed("camera_change_left"):
		rotation.y = deg2rad(rad2deg(rotation.y) + 2)
	if Input.is_action_pressed("camera_change_right"):
		rotation.y = deg2rad(rad2deg(rotation.y) - 2)
	
func debug_target(aim):
	if $print_timer.time_left == 0:
		$print_timer.start()
		
		print('aim:', aim)
		
func _physics_process(delta: float) -> void:
	
	var direction_ground := Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_down") - Input.get_action_strength("move_up"))
	
	var move = false
	var target_dir = $RotationOffset/Target.rotation_degrees.y
	
	
	if abs(target_dir) > 135:
		direction = 0
	elif abs(target_dir) < 45:
		direction = 1
	elif target_dir < 0:
		direction = 2
	else:
		direction = 3
	
	
	if direction_ground.y > 0:
		move = true
		#direction = 0
	elif direction_ground.y < 0:
		move = true
		#direction = 1
	elif direction_ground.x > 0:
		move = true
		#direction = 2
	elif direction_ground.x < 0:
		move = true
		#direction = 3
		
	direction_ground = direction_ground.rotated(-rotation.y).normalized()
	
	if not is_on_floor():
		velocity_y -= gravity * delta
	
	var velocity = Vector3(
		direction_ground.x * speed,
		velocity_y,
		direction_ground.y * speed)
		
	move_and_slide(velocity, FLOOR_NORMAL)
	
	
	velocity = Vector2(direction_ground.x, direction_ground.y).rotated(deg2rad(-45))
	
	if last_velocity != velocity:
		last_velocity = velocity
	

	move_animation(move)
	
	if is_on_floor() or is_on_ceiling():
		velocity_y = 0.0
	
	
	

func move_animation(move):
	if move:
		if $Animation.time_left == 0:
			$RotationOffset/Sprite3D.frame = ( $RotationOffset/Sprite3D.frame + 1 ) % 4 + direction * 4
			$Animation.start(0)
	else:
		$RotationOffset/Sprite3D.frame = 0 + direction * 4
		
var camera_angles = [-45, -90, -135, 180, 135, 90, 45, 0]
var camera_index = 0

#func _unhandled_input(event: InputEvent) -> void:
#	if event.is_action_pressed("jump"):
#		if is_on_floor():
#			velocity_y = jump_force
#	#if event.is_action_pressed("camera_change"):
#	#	camera_index = ( camera_index + 1 ) % camera_angles.size()
#	#	rotation.y = deg2rad(camera_angles[ camera_index ])
#	if event.is_action("camera_change_left"):
#		rotation.y = deg2rad(rad2deg(rotation.y) + 2)
#	if event.is_action("camera_change_right"):
#		rotation.y = deg2rad(rad2deg(rotation.y) - 2)
	
		
		
func _on_Spatial_body_entered(body):
	print(body)
