extends KinematicBody

export var path_name = ""
export var move = false
export var speed = 0
var path
var path_index = 0
var target = Vector3()
var velocity = Vector3()
# Called when the node enters the scene tree for the first time.
func _ready():
	if move:
		path = get_parent().get_node(path_name).get_children()
		target = path[0].translation

	
func _physics_process(delta):
	if move:
		velocity = (target - translation).normalized() * speed
		if (target - translation).length() > 1:
			move_and_slide(velocity)
	
			
func _on_Cage_Up():
	target = path[1].translation

func _on_Cage_Down():
	target = path[0].translation