extends Area

var destination

func _ready():
	destination = get_parent().destination
    

func _physics_process(delta):
	var bodies = get_overlapping_bodies()

	for curBody in bodies:
		if curBody.get_name() == 'Player':
			print('Change map to: ' + destination)