extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	visible = PlayerVariables.sword_active

func _process(delta):
	visible = PlayerVariables.sword_active
	if (Input.is_action_pressed("atk")):
		$AnimationPlayer.play("default")
		$AnimationPlayer.seek(0, true)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
