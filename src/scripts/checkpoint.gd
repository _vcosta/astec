extends Area

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var main_scene = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_Checkpoint_body_entered(body):
	if body.get_name() == 'Player':
		if main_scene:
			PlayerVariables.main_scene_checkpoint.origin = $Position3D.get_global_transform().origin
		else:	
			get_parent().checkpoint.origin = $Position3D.get_global_transform().origin
