extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var destination = ''

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_Area_body_entered(body):
	if body.get_name() == 'Player':
			$Sprite3D.visible = true
			


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action("action") and $Sprite3D.visible :
		get_tree().change_scene("res://src/scenes/" + destination + ".tscn")

func _on_Area_body_exited(body):
	if body.get_name() == 'Player':
			$Sprite3D.visible = false
