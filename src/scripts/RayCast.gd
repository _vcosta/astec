extends RayCast

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var rotation_angle
var player
var up_dir
var cur_dir
var target_dir
var destination

# Called when the node enters the scene tree for the first time.
func _ready():
	destination = get_parent().destination
	player = get_parent().get_node("Player")
	rotation_angle = Vector3(0, 0, 0)
	
func _process(delta):
	if is_colliding():
		var object = get_collider()
		if object.get_name() == "Player":
			print('destination:' + destination)
	
				
func look_follow(state, current_transform, target_position):
	up_dir = Vector3(0, 1, 0)
	cur_dir = current_transform.basis.xform(Vector3(0, 0, 1))
	target_dir = (target_position - current_transform.origin).normalized()
	if target_position.z > current_transform.origin.z:
		rotation_angle = acos(cur_dir.x) - acos(target_dir.x)
	else:
		rotation_angle = acos(target_dir.x) - acos(cur_dir.x) 
	state.set_angular_velocity(up_dir * (rotation_angle / state.get_step()))
	
func _integrate_forces(state):
	var target_position = player.get_global_transform().origin
	look_follow(state, get_global_transform(), target_position)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
