extends RigidBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

var rotation_angle
var player
var up_dir
var cur_dir
var target_dir
# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_parent().get_node("Player")
	rotation_angle = Vector3(0, 0, 0)

func _process(delta):
	if $Attack_cooldown.time_left == 0:
		var ray = $RayCast
		if ray.is_colliding():
			var object = ray.get_collider()
			if object.get_name() == "Player":
				attack()
	
			
			
func attack():
	$Animator/AnimationPlayer.play("attack")
	$Attack_cooldown.start()
	player.life -= 15
		
func look_follow(state, current_transform, target_position):
	up_dir = Vector3(0, 1, 0)
	cur_dir = current_transform.basis.xform(Vector3(0, 0, 1))
	target_dir = (target_position - current_transform.origin).normalized()
	if target_position.z > current_transform.origin.z:
		rotation_angle = acos(cur_dir.x) - acos(target_dir.x)
	else:
		rotation_angle = acos(target_dir.x) - acos(cur_dir.x) 
	state.set_angular_velocity(up_dir * (rotation_angle / state.get_step()))
	
	var player_rotation = rad2deg(player.rotation.y)

	
	if player_rotation - rotation_degrees.y > 0:
		$Animator.scale.x = -1
	else:
		$Animator.scale.x = 1
	#print(rad2deg(Vector2(translation.x - player.translation.x, translation.z-player.translation.z).angle()))
	
	
	#DrawLine3D.DrawLine(translation, a, Color(0, 0, 1), 0)
	
func _integrate_forces(state):
	var target_position = player.get_global_transform().origin
	look_follow(state, get_global_transform(), target_position)
	
	
	

	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
