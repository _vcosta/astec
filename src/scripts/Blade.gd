extends RigidBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const FLOOR_NORMAL = Vector3(0.0, 1.0, 0.0)
var speed = 3
var dir_x = 1
var dir_y = 1
var velocity = Vector3(
		dir_x * 1 * speed,
		0,
		dir_y * speed)
		
var t
# Called when the node enters the scene tree for the first time.
func _ready():
	t = get_transform()
	
func _process(delta):
	t.origin += velocity * delta
	set_transform(t)
	$Position3D/Sprite3D.rotation_degrees = $Position3D/Sprite3D.rotation_degrees + Vector3(0, 0, 5)


func _on_change_dir_timeout():
	dir_x *= -1
	dir_y *= -1
	velocity = Vector3(
		dir_x * 1 * speed,
		0,
		dir_y * speed)
	$change_dir.start(-1)
	

