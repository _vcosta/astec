extends Position3D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player
# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_parent().get_parent().get_node("Player")
	
func _process(delta):
	rotation_degrees = player.rotation_degrees
	rotation_degrees.y -= get_parent().rotation_degrees.y
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
